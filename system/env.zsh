export EDITOR=gvim
export CHROME_BIN="/usr/bin/google-chrome-stable"
# for xfce terminal 256 colors
export TERM=xterm-256color

# Setting ag as the default source for fzf
# export FZF_DEFAULT_COMMAND='ag -l -g ""'
