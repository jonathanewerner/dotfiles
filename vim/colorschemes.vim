" Plugin tomasr/molokai
" Plugin sickill/vim-monokai
" Plugin quanganhdo/grb256
" Plugin xoria256.vim
" Plugin Wombat
" Plugin altercation/vim-colors-solarized

" colorscheme xoria256
" colorscheme grb256
Plugin morhetz/gruvbox
colorscheme gruvbox
set background=dark
" set background=light
" if strftime("%H") < 12
"   set background=light
" else
"   set background=dark
" endif
